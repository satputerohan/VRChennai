﻿using UnityEngine;
using UnityEngine.iOS;
using UnityEngine.EventSystems;

public class RulerTest : MonoBehaviour, IDragHandler, IBeginDragHandler {
    [SerializeField] Camera mapCam;
    float minZoomSize, maxZoomSize, posDelta;
    Vector3 camStartPos, mousePos, prevPos;
    [SerializeField] float zoomDamp;
    bool zoomingIn;
    Vector4 mappedLimits;
    Vector2 zoomPoint;

    private float lastDist = 0;

    private float curDist = 0;

    Vector2 initialMidpoint = new Vector2();
    Vector2 initialSceneMidPoint = new Vector2();

    void Start () {
        ContextInfo.Instance.cam = mapCam;
        camStartPos = mapCam.transform.position;
        maxZoomSize = 100f;
        minZoomSize = 30f;
        mousePos = camStartPos;
        zoomPoint = camStartPos;
        mappedLimits = new Vector4(134f, 134f, 100f,100f);

    }

    void Update()
    {
#if UNITY_IOS
        
        if (Input.touchCount > 1 )

        {
            //Two finger touch does pinch to zoom
            
            var touch1 = Input.GetTouch(0);
            var touch2 = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touch1PrevPos = touch1.position - touch1.deltaPosition;
            Vector2 touch2PrevPos = touch2.position - touch2.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touch1PrevPos - touch2PrevPos).magnitude;
            float touchDeltaMag = (touch1.position - touch2.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
           
           

            //Get the overall Scale of the camera.
            float cameraScale = ((mapCam.orthographicSize) / ((float)Screen.height / 2));

            //Get the Camera positions (this just prevents you from having to call "GetComptenent<Camera>().tra. . ." repeatedlly) 
            float cameraPositionX = mapCam.transform.position.x;
            float cameraPositionY = mapCam.transform.position.y;

            //Checking to see if either of the touches began
            if (touch1.phase == TouchPhase.Began || touch2.phase == TouchPhase.Began)
            {


                //Get the inital midpoint when you touch your fingers.  This will be in "Screen" coordinates
                initialMidpoint = new Vector2(((touch2.position.x + touch1.position.x) / 2) - Screen.width / 2,
                                               ((touch2.position.y + touch1.position.y) / 2) - Screen.height / 2);

                //Converts the inital midpoint from "Screen" to "scene" coordinates
                initialSceneMidPoint = new Vector2((cameraPositionX + (initialMidpoint.x * cameraScale)),
                                                    (cameraPositionY + (initialMidpoint.y * cameraScale)));
            }


            //Get the current midpoint of the touches in "Screen" coordinates
            Vector2 midpoint = new Vector2(((touch2.position.x + touch1.position.x) / 2) - Screen.width / 2,
                                           ((touch2.position.y + touch1.position.y) / 2) - Screen.height / 2);

            
            mapCam.orthographicSize += deltaMagnitudeDiff * zoomDamp;
            mapCam.orthographicSize = Mathf.Clamp(mapCam.orthographicSize, minZoomSize, maxZoomSize);

            mappedLimits = GetMappedPosLimits();

            //Moves the camera depending on how far off your current midpoint is from the initial midpoint
            //This means that if you don't change the midpoint while you zoom that point will stay in the same x,y location on the screen
            mapCam.transform.position = new Vector3(initialSceneMidPoint.x - (midpoint.x * cameraScale),
                                                                        initialSceneMidPoint.y - (midpoint.y * cameraScale),
                                                                        0);
            mapCam.transform.position = new Vector3(Mathf.Clamp(mapCam.transform.position.x, mappedLimits.x, mappedLimits.y),
                                                Mathf.Clamp(mapCam.transform.position.y, mappedLimits.z, mappedLimits.w));

            
        }



#endif

#if UNITY_STANDALONE
        if (Input.mouseScrollDelta != Vector2.zero)
        {
            mapCam.orthographicSize -= Input.GetAxis("Mouse ScrollWheel") * zoomDamp;
            mapCam.orthographicSize = Mathf.Clamp(mapCam.orthographicSize, minZoomSize, maxZoomSize);
            mappedLimits = GetMappedPosLimits();
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            
            if (!zoomingIn)
            {
                zoomingIn = true;
                mousePos = mapCam.ScreenToWorldPoint(Input.mousePosition);
            }
            
            mapCam.transform.position = Vector3.Lerp(mapCam.transform.position, mousePos, Time.deltaTime * 12f);
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            zoomingIn = false;
            
            mapCam.transform.position = Vector3.Lerp(mapCam.transform.position, camStartPos, Time.deltaTime * 10f);
        }
#endif
    }

    public void OnBeginDrag(PointerEventData pData)
    {
        if (Input.touchCount == 1)
        {
            prevPos = mapCam.ScreenToWorldPoint(pData.position);
        }
    }

    public void OnDrag(PointerEventData pData)
    {
        if (Input.touchCount == 1)
        {
            mappedLimits = GetMappedPosLimits();
            mapCam.transform.position -= (mapCam.ScreenToWorldPoint(pData.position) - prevPos);

            mapCam.transform.position = new Vector3(Mathf.Clamp(mapCam.transform.position.x, mappedLimits.x, mappedLimits.y),
                                                Mathf.Clamp(mapCam.transform.position.y, mappedLimits.z, mappedLimits.w));


            prevPos = mapCam.ScreenToWorldPoint(pData.position);
        }
    }
    
    Vector4 GetMappedPosLimits()
    {
        var mappedOrthoSize = Mathf.InverseLerp(30f, 100f, mapCam.orthographicSize);
        var mappedXMinPos = Mathf.Lerp(39f, 134f, mappedOrthoSize);
        var mappedXMaxPos = Mathf.Lerp(228f, 134f, mappedOrthoSize);
        var mappedYMinPos = Mathf.Lerp(29f, 100f, mappedOrthoSize);
        var mappedYMaxPos = Mathf.Lerp(172f, 100f, mappedOrthoSize);
        return new Vector4(mappedXMinPos, mappedXMaxPos, mappedYMinPos, mappedYMaxPos);
    }

    void OnGUI () {
        if (UIManager.instance.currentPageBn == EnumCollections.ClickableButtons.Floorplans)
        {
            ContextInfo.Instance.LoadContextInfo();
            Rulers.Instance.DrawRulers();
        }
	}
}
