﻿
public static class EnumCollections
{
    public enum ClickableButtons
    {
        None,
        //Dropdown
        Home,
        Catchment,
        Connectivity,
        Floorplans,
        DDCount,
        //Non Dropdown
        Commercial,//not used
        Gallery,
        Info,
        PageCount,
        MenuToggle,
        //Gallery Pic
        GalleryNext,
        CatchmentInfo,
        GalleryPrev,
        Brochure,
        PlayVid,
		ResetCam,
    }

    public enum HomeSubMenu
    {
        Amenities,
        Accessibility,
        None,
    }

    public enum CatchmentSubMenu
    {
        AnnaNagar,
        Chennai,
        None,
    }

    public enum Amenities
    {
        EastEntrance,
        SouthEntrance,
        Street,
        RoofTennis,
        RoofSwim,
        RoofJog,
        EventSpace,
        Playground,
        WestEntrance,
        Street1,
        None,
        Close,
        
    }


    public enum FloorplansSubMenu
    {
        LevelStacking,
        GroundLevel,
        Level1,
        Level2,
        Level3,
        RoofLevel,
        None,
        Basement,
    }

    public enum CatchmentBns
    {
        SuperPrimary,
        Primary,
        Secondary,
        None,
    }
}
