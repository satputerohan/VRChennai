﻿using UnityEngine;
using System.Collections;

public class RegisterToManager : MonoBehaviour {

	void Start () {
        if (transform.CompareTag("AccessPoints"))
        {
            UIManager.instance.accessPoints = gameObject;
            gameObject.SetActive(false);
        }
        else if(transform.CompareTag("Accessibility"))
        {
            UIManager.instance.accessibilityPoints = gameObject;
            gameObject.SetActive(false);
        }
        else if (transform.CompareTag("Connectivity"))
        {
            UIManager.instance.connectivityPoints = gameObject;
            gameObject.SetActive(false);
        }
    }
	
}
