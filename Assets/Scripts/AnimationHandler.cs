﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Collections;

public class AnimationHandler : MonoBehaviour {
    public static AnimationHandler Singleton;

    public Animator stackBG, stackSubMenu;
    public List<GameObject> Stacks;
    public List<Animator> stackMenu;
    [SerializeField] [Range(0f, 1f)] float animGranularity = 0.1f;
    public GameObject activeStack, blocker;
    public bool processing;

    void Awake () {
        Singleton = this;
	}

    void OnEnable()
    {
        EventDispatcher.Singleton.onStackSelected += OnStackSelected;
    }

    void OnDisable()
    {
        EventDispatcher.Singleton.onStackSelected -= OnStackSelected;
    }

    public IEnumerator PlayAnim(List<Animator> stackMenu, bool isForward)
    {
        for (int i = 0; i < stackMenu.Count; i++)
        {
            yield return new WaitForSeconds(animGranularity);
            stackMenu[i].SetBool("SubMenuPop", isForward);
        }
    }

    public IEnumerator PlayAnim (Animator anim, string param, float delay, bool isForward)
    {
        yield return new WaitForSeconds(delay);
        anim.SetBool(param, isForward);
        if(!isForward)
        {
            yield return new WaitForSeconds(1.5f);
            var stackRoot = stackBG.transform.parent.gameObject;
            stackRoot.SetActive(false);
        }
    }

    public IEnumerator PlayAnim(GameObject anim, float delay, bool isForward)
    {
        yield return new WaitForSeconds(delay);

        for (int i = 0; i < anim.transform.childCount - 1; i++)
        {
            yield return new WaitForSeconds(animGranularity);
            anim.transform.GetChild(i).GetComponent<Animator>().SetBool("Slide", isForward);
        }

        yield return new WaitForSeconds(animGranularity);
        var currentAnim = anim.transform.GetChild(anim.transform.childCount - 1).GetComponent<Animator>();
        if (currentAnim.gameObject.activeSelf)
        {
            currentAnim.SetBool("TextFade", isForward);
        }

        if (!isForward)
        {
            yield return new WaitForSeconds(1f);
            if(blocker.activeSelf)
            {
                if(!UIManager.instance.GetRulerStatus() && UIManager.instance.currentPageBn == EnumCollections.ClickableButtons.Floorplans)
                {
                    UIManager.instance.SetRulerStatus(true);
                }
                blocker.SetActive(false);
            }
            anim.SetActive(false);
        }
    }

    void OnStackSelected(int stackNum)
    {
        ResetActiveStack();
        if(stackNum > 5)
        {
            stackNum = 5;
        }

        Stacks[stackNum].SetActive(true);
        activeStack = Stacks[stackNum];
        StartCoroutine(PlayAnim(Stacks[stackNum], 0f, true));
    }

    public void SetScreens()
    {
        EventDispatcher.Singleton.StackSelected(EnumCollections.FloorplansSubMenu.GroundLevel);
        StartCoroutine(PlayAnim(stackSubMenu, "StackReady", 0f, true));
        StartCoroutine(PlayAnim(stackBG, "Slide", 0f, true));        
    }

    public void ResetScreens()
    {
        activeStack.SetActive(false);
        var stackRoot = stackBG.transform.parent.gameObject;
        stackRoot.SetActive(false);
    }

    public void ResetActiveStack()
    {
        if (activeStack && activeStack.activeSelf)
        {            
            var images = activeStack.GetComponentsInChildren<Image>();
            foreach (Image img in images)
            {
                img.fillAmount = 0f;
            }
            activeStack.GetComponentInChildren<CanvasGroup>().alpha = 0f;
            activeStack.SetActive(false);
        }
    }
}
