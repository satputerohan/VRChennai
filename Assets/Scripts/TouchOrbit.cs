using UnityEngine;

using System.Collections;



public class TouchOrbit : MonoBehaviour
{

    public Transform target;

    public float distance;

    public int cameraSpeed = 5;



    public float xSpeed = 50.0f;

    public float ySpeed = 35.0f;

    public float pinchSpeed, panSpeed;

    private float lastDist = 0;

    private float curDist = 0;

    private float lowPassFilter = 10f;

    public int yMinLimit = 10; //Lowest vertical angle in respect with the target.

    public int yMaxLimit = 60;



    public float minDistance ; //Min distance of the camera from the target

    public float maxDistance ;



    private float x = 0.0f, startX = 0.0f;

	private float y = 0.0f, startY = 0.0f;

    private Touch touch;

    Vector3 targetStartPos;

	float startDistance;

    Camera cam;

	bool reset;

    void OnDisable()
    {
        if (target)
        {
            target.position = targetStartPos;
        }
    }

    void Start()
    {

		startDistance = distance;
        Vector3 angles = transform.eulerAngles;

        if(target)
        {
            targetStartPos = target.position;
        }

		x = startX= angles.y;

        y = startY= angles.x;

        cam = GetComponent<Camera>();

        // Make the rigid body not change rotation

        //if (rigidbody)

        //    rigidbody.freezeRotation = true;

    }

	public void OnReset()
	{
		reset = true;
	}

    void Update()
    {

        if (!reset && target && cam != null)
        {
            //Zooming with mouse

            //distance += Input.GetAxis("Mouse ScrollWheel") * distance;

            if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved)

            {

                //One finger touch does orbit

                touch = Input.GetTouch(0);

                if (touch.deltaPosition.magnitude > 1f)
                {
                    x += touch.deltaPosition.x * xSpeed * 0.02f;

                    y -= touch.deltaPosition.y * ySpeed * 0.02f;
                }

            }

            if (Input.touchCount > 1 && (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(1).phase == TouchPhase.Moved))

            {

                //Two finger touch does pinch to zoom

                var touch1 = Input.GetTouch(0);

                var touch2 = Input.GetTouch(1);

                // Find the position in the previous frame of each touch.
                Vector2 touchZeroPrevPos = touch1.position - touch1.deltaPosition;
                Vector2 touchOnePrevPos = touch2.position - touch2.deltaPosition;

                // Find the magnitude of the vector (the distance) between the touches in each frame.
                float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                float touchDeltaMag = (touch1.position - touch2.position).magnitude;

                // Find the difference in the distances between each frame.
                float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

                if (touchDeltaMag.AlmostEquals(prevTouchDeltaMag, lowPassFilter) && (Input.GetTouch(0).phase == TouchPhase.Moved && Input.GetTouch(1).phase == TouchPhase.Moved))
                {
                    //Panning
                    var touchAvgCurrent = (touch1.position + touch2.position) * 0.5f;
                    var touchAvgPrev = (touchZeroPrevPos + touchOnePrevPos) * 0.5f;
                    var touchAvgDelta = touchAvgCurrent - touchAvgPrev;

					var targetOffset = -(transform.right * touchAvgDelta.x) * panSpeed * 0.1f;
					var resultant = target.position + targetOffset;

					if( resultant.x >= 85f && resultant.x <= 105f &&
						resultant.z >= 0f && resultant.z <= 40f)
					{
						target.Translate(targetOffset);	
					}


                }
                else
                {
                    if (deltaMagnitudeDiff < -lowPassFilter)

                    {

                        distance -= Vector2.Distance(touch1.deltaPosition, touch2.deltaPosition) * pinchSpeed * 0.1f;

                    }
                    else if(deltaMagnitudeDiff > lowPassFilter)
                    {

                        distance += Vector2.Distance(touch1.deltaPosition, touch2.deltaPosition) * pinchSpeed * 0.1f;

                    }
                    
                    distance = Mathf.Clamp(distance, minDistance, maxDistance);
                }
            }

            //Detect mouse drag;
//
//            if (Input.GetMouseButton(0))
//            {
//
//                x += Input.GetAxis("Mouse X") * xSpeed * 0.02f;
//
//                y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;
//
//            }

            y = ClampAngle(y, yMinLimit, yMaxLimit); 



            Quaternion rotation = Quaternion.Euler(y, x, 0);

            Vector3 vTemp = new Vector3(0.0f, 0.0f, -distance);

            Vector3 position = rotation * vTemp + target.position;



            transform.position = position;//Vector3.Lerp(transform.position, position, cameraSpeed * Time.deltaTime);;

            transform.rotation = rotation;

        }

		else if(reset)
		{
			

			target.position = targetStartPos;
			distance = startDistance;

//			var angles = transform.eulerAngles;
//
			x = startX;
			y = startY;

			Quaternion rotation = Quaternion.Euler(startY, startX, 0);

			Vector3 vTemp = new Vector3(0.0f, 0.0f, -distance);

			Vector3 position = rotation * vTemp + target.position;

			transform.position = position;
			transform.rotation = rotation;


			reset = false;
		}

    }



    static float ClampAngle(float angle, float min, float max)
    {

        if (angle < -360)

            angle += 360;

        if (angle > 360)

            angle -= 360;

        return Mathf.Clamp(angle, min, max);

    }



}