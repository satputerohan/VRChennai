﻿using UnityEngine;
using UnityEngine.EventSystems;

public class InputHandler : MonoBehaviour, IPointerClickHandler {

    [SerializeField] EnumCollections.ClickableButtons clickedBn = EnumCollections.ClickableButtons.None;
    public EnumCollections.HomeSubMenu home = EnumCollections.HomeSubMenu.None;
    public EnumCollections.CatchmentSubMenu catchment = EnumCollections.CatchmentSubMenu.None;
    public EnumCollections.FloorplansSubMenu floorplans = EnumCollections.FloorplansSubMenu.None;
    public EnumCollections.FloorplansSubMenu stackingMenu = EnumCollections.FloorplansSubMenu.None;
    public EnumCollections.Amenities amenity = EnumCollections.Amenities.None;

    public void OnPointerClick(PointerEventData pData)
    {
        if(clickedBn != EnumCollections.ClickableButtons.None)
        {
            EventDispatcher.Singleton.ButtonClicked(clickedBn);
        }
        else if(home != EnumCollections.HomeSubMenu.None)
        {
            EventDispatcher.Singleton.SubMenuSelected(transform.parent.GetSiblingIndex());
        }
        else if(catchment != EnumCollections.CatchmentSubMenu.None)
        {
            EventDispatcher.Singleton.SubMenuSelected(transform.parent.GetSiblingIndex());
        }
        else if(floorplans != EnumCollections.FloorplansSubMenu.None)
        {
            EventDispatcher.Singleton.SubMenuSelected(transform.parent.GetSiblingIndex());
        }
        else if(stackingMenu != EnumCollections.FloorplansSubMenu.None)
        {
            EventDispatcher.Singleton.StackSelected(stackingMenu);
        }
        else if(amenity != EnumCollections.Amenities.None)
        {
            EventDispatcher.Singleton.AmenitySelected(amenity);
		}

    }
}
