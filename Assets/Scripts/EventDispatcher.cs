﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class EventDispatcher : MonoBehaviour {

    private static EventDispatcher _singleton;

    public static EventDispatcher Singleton
    {
        get { return _singleton ?? (_singleton = GameObject.FindGameObjectWithTag("Manager").GetComponent<EventDispatcher>()); } 
    }

    public delegate void OnInputButtonClicked(EnumCollections.ClickableButtons clickedBn);
    public delegate void OnSubMenuSelected(int subMenuNum);
    public delegate void OnStackSelected(int stackNum);
    public delegate void OnAmenitySelected(EnumCollections.Amenities amenity);
    public delegate void OnAnimFinished();

    public event OnInputButtonClicked onButtonClicked;
    public event OnSubMenuSelected onSubMenuSelected;
    public event OnStackSelected onStackSelected;
    public event OnAmenitySelected onAmenitySelected;
    public event OnAnimFinished onAnimFinished;

    public void ButtonClicked(EnumCollections.ClickableButtons clickedBn)
    {
        if(onButtonClicked != null)
        {
            onButtonClicked(clickedBn);
        }
    }

    public void SubMenuSelected(int subMenuNum)
    {
        if(onSubMenuSelected != null)
        {
            onSubMenuSelected(subMenuNum);
        }
    }

    public void StackSelected(EnumCollections.FloorplansSubMenu selectedStack)
    {
        if(onStackSelected != null)
        {
            onStackSelected((int)selectedStack - 1);
        }
    }

    public void AmenitySelected(EnumCollections.Amenities selectedAmenity)
    {
        if(onAmenitySelected!= null)
        {
            onAmenitySelected(selectedAmenity);
        }
    }

    internal void AnimFinished()
    {
        if(onAnimFinished != null)
        {
            onAnimFinished();
        }
    }
}
