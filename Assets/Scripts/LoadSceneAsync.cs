﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneAsync : MonoBehaviour
{

    public AsyncOperation asyncOp;
    public bool animFinished;

    void Start()
    {

        asyncOp = SceneManager.LoadSceneAsync(1);
        asyncOp.allowSceneActivation = false;
    }

    void Update()
    {
        if (animFinished)
        {
            asyncOp.allowSceneActivation = true;
        }
    }

}
