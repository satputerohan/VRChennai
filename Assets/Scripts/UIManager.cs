﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class UIManager : MonoBehaviour {
    public static UIManager instance;

    [SerializeField] Animator taskAnim, infoAnim;
    [SerializeField] Sprite showMenu, hideMenu, selectedBn, baseSprite;
    [SerializeField] Image menuToggle, amenityImg;
    [SerializeField] GameObject  compass, subMenu, stackRoot, blocker, catchmentRoot, accessibilityLegends, connectivityLegends, brochureBn;
    [SerializeField] GameObject gallery, infoToggleBn, infoVR, infoPanel, rulerDetect, playVid, frame, brochure, popUp;
    [SerializeField] List<Sprite> pics, floorPlanBGs, homeBGs, catchmentBGs, infoPanelBGs, infoPages, galleryBGs, amenityPics;
    [SerializeField] Image pageBG, mapBG;
    [SerializeField] RulerTest ruler;
    [SerializeField] Color selectedColour;
    [SerializeField] List<GameObject> subMenuSizes;
    [SerializeField] List<Color> subMenuColours;
	CanvasScaler popUpCanvas;
    int imageNum;
    List<string> subMenuItemsList = new List<string>
        {
            "0.1|0.1",
            "1.0|0.1",
            "1.0.0.0.2.2|2.3.1.9.5.6",
        };
    List<Text> taskbarBnTexts;
    Color prevBnColour;
    public List<Vector3> startPosList;
    public Camera MainCam, mapCam;
    GameObject stackInstance, currentPopUpBn;
    [HideInInspector]
    public EnumCollections.ClickableButtons currentPageBn;
    bool toggleMenu, toggleInfo, brochureOn;
    Image gallerybg, prevBnImage;
    [SerializeField] List<string> homeOptions, floorOptions, catchmentOptions, connectivityOptions, amenityOptions;
    public bool isStackVisible;
    public GameObject accessPoints, accessibilityPoints, connectivityPoints;
    RulerTest catchmentZoom;
    Vector3 mapCamStartPos, framePos, popUpWorldPos, prevCamRot, currentCamRot;
    Text amenityText;
    public List<GameObject> towerCollidersActive;
    RectTransform frameRect, frame3DTarget, popUpRect, popUpTarget;
    public Text brochureText;

    void Awake()
    {
        instance = this;
    }

    void OnEnable()
    {
        EventDispatcher.Singleton.onButtonClicked += OnButtonClicked;
        EventDispatcher.Singleton.onSubMenuSelected += OnSubMenuSelected;
        EventDispatcher.Singleton.onAmenitySelected += OnAmenitySelected;
    }

    void OnDisable()
    {
        EventDispatcher.Singleton.onButtonClicked -= OnButtonClicked;
        EventDispatcher.Singleton.onSubMenuSelected -= OnSubMenuSelected;
        EventDispatcher.Singleton.onAmenitySelected -= OnAmenitySelected;
    }
    
	void Start () {
        ruler.enabled = false;
        rulerDetect.SetActive(false);
        currentPageBn = EnumCollections.ClickableButtons.Home;
        PopulateSubmenu(currentPageBn);
        gallerybg = gallery.GetComponent<Image>();
        catchmentZoom = catchmentRoot.GetComponent<RulerTest>();
        infoPanel = infoAnim.gameObject;
        mapCamStartPos = mapCam.transform.position;
        imageNum = 0;
        toggleMenu = true;
        toggleInfo = true;
        infoToggleBn.SetActive(false);
        mapCam.clearFlags = CameraClearFlags.Depth;
        taskbarBnTexts = taskAnim.transform.GetComponentsInChildren<Text>().ToList();
        startPosList = new List<Vector3>{Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero};
        amenityImg = frame.transform.Find("Pic").GetComponent<Image>();
        amenityText = frame.transform.Find("TextBG").GetChild(0).GetComponent<Text>();
        frameRect = frame.GetComponent<RectTransform>();
		popUpRect = popUp.GetComponent <RectTransform> ();
		popUpCanvas = popUp.GetComponentInParent <CanvasScaler> ();
		popUpCanvas.referenceResolution = new Vector2(Screen.width, Screen.height);
		prevCamRot = MainCam.transform.eulerAngles;
		currentCamRot = MainCam.transform.eulerAngles;
		SetCompass ();
    }

	void SetCompass()
	{
		
		compass.transform.Rotate (Vector3.forward * (currentCamRot.y - prevCamRot.y));
		prevCamRot = currentCamRot;
	}

    void Update()
    {
        if (MainCam == null && Camera.main != null)
        {
            MainCam = Camera.main;
        }
        if(ruler.enabled && !rulerDetect.activeSelf)
        {
            rulerDetect.SetActive(true);
        }
        else if(!ruler.enabled && rulerDetect.activeSelf)
        {
            rulerDetect.SetActive(false);
        }

        if(frame.activeSelf)
        {
			framePos = frame3DTarget.position;
			var temp = MainCam.WorldToScreenPoint(framePos);
			frameRect.anchoredPosition = new Vector2(temp.x - (popUpCanvas.referenceResolution.x * 0.5f), temp.y - (popUpCanvas.referenceResolution.y * 0.5f));
        }

		if(popUp.activeSelf)
		{
			popUpWorldPos = popUpTarget.position;
			var temp = MainCam.WorldToScreenPoint (popUpWorldPos);
			popUpRect.anchoredPosition = new Vector2(temp.x - (popUpCanvas.referenceResolution.x * 0.5f), temp.y - (popUpCanvas.referenceResolution.y * 0.5f));
		}

    }

	void LateUpdate()
	{
		currentCamRot = MainCam.transform.eulerAngles;
		SetCompass ();
	}

	void ResetAll()
	{
		foreach(Text text in taskbarBnTexts)
		{
			text.color = Color.white;
		}
		brochureText.color = Color.white ;
		gallery.SetActive(false);
		ruler.enabled = false;
		isStackVisible = false;
		infoVR.SetActive(false);
		playVid.SetActive(false);
		popUp.SetActive (false);
		compass.SetActive (false);
		catchmentZoom.enabled = false;
		accessPoints.SetActive(false);
		accessibilityPoints.SetActive(false);
		accessibilityLegends.SetActive(false);
		connectivityLegends.SetActive (false);
		connectivityPoints.SetActive(false);
		infoToggleBn.SetActive(false);
		brochure.SetActive(false);
		ResetAccessPoints();
	}


    void OnButtonClicked (EnumCollections.ClickableButtons clickedBn)
    {
        if ((int)clickedBn < (int)EnumCollections.ClickableButtons.PageCount)
        {
			currentPageBn = clickedBn;
			ResetAll ();
            for (int i = 0; i < catchmentOptions.Count; i++)
            {
                infoPanel.transform.GetChild(i).gameObject.SetActive(false);
                catchmentRoot.transform.GetChild(i).gameObject.SetActive(false);
            }
            if (!toggleInfo)
            {
                infoAnim.SetBool("ToggleVisibility", true);
            }
            if ((int)clickedBn < (int)EnumCollections.ClickableButtons.DDCount)
            {
                taskbarBnTexts[(int)clickedBn - 1].color = selectedColour;
                subMenu.gameObject.SetActive(true);
                PopulateSubmenu(currentPageBn);
                if (currentPageBn == EnumCollections.ClickableButtons.Floorplans)
                {
                    OnSubMenuSelected(5);
                }
                else
                {
                    OnSubMenuSelected(0);
                }

                if(currentPageBn == EnumCollections.ClickableButtons.Connectivity)
                {
                    connectivityPoints.SetActive(true);
                }
            }
            else
            {
                if (stackRoot.activeSelf)
                {
                    AnimationHandler.Singleton.ResetActiveStack();
                    AnimationHandler.Singleton.ResetScreens();
                }
                if (MainCam.enabled)
                {
                    MainCam.enabled = false;
                    mapCam.clearFlags = CameraClearFlags.SolidColor;
                }
                Debug.Log("here");
                taskbarBnTexts[(int)clickedBn - 3].color = selectedColour;
                subMenu.gameObject.SetActive(false);
                switch (clickedBn)
                {
                    case EnumCollections.ClickableButtons.Gallery:
                        gallery.SetActive(true);
                        playVid.SetActive(true);
                        pageBG.enabled = false;
                        mapBG.enabled = false;
                        break;
                    case EnumCollections.ClickableButtons.Info:
                        pageBG.enabled = true;
                        mapBG.enabled = false;
                        pageBG.sprite = infoPages[0];
                        brochureBn.SetActive(true);
                        infoVR.SetActive(true);
                        break;
                    default:
                        break;
                }
            }
        }
        else
        {
            switch (clickedBn)
            {
				case EnumCollections.ClickableButtons.ResetCam:
					
					break;
                case EnumCollections.ClickableButtons.MenuToggle:
                    ToggleMenu();
                    break;
                case EnumCollections.ClickableButtons.GalleryNext:
                    if (imageNum < galleryBGs.Count-1)
                    {
                        imageNum++;
                    }
                    else
                    {
                        imageNum = 0;
                    }
                    DisplayPic(imageNum);
                    break;
                case EnumCollections.ClickableButtons.GalleryPrev:
                    if (imageNum > 0)
                    {
                        imageNum--;
                    }
                    else
                    {
                        imageNum = galleryBGs.Count - 1;
                    }
                    DisplayPic(imageNum);
                    break;
                case EnumCollections.ClickableButtons.CatchmentInfo:
                    ToggleInfo();
                    break;
                case EnumCollections.ClickableButtons.Brochure:

                    //if (!brochureOn)
                    //{

                    if (stackRoot.activeSelf)
                    {
                        AnimationHandler.Singleton.ResetActiveStack();
                        AnimationHandler.Singleton.ResetScreens();
                    }
                    if (MainCam.enabled)
                    {
                        MainCam.enabled = false;
                        mapCam.clearFlags = CameraClearFlags.SolidColor;
                    }
                   
					ResetAll ();
					brochure.SetActive(true);
                    for (int i = 0; i < catchmentOptions.Count; i++)
                    {
                        infoPanel.transform.GetChild(i).gameObject.SetActive(false);
                        catchmentRoot.transform.GetChild(i).gameObject.SetActive(false);
                    }
                    if (!toggleInfo)
                    {
                        infoAnim.SetBool("ToggleVisibility", true);
                    }
                    brochureText.color = selectedColour;
                    subMenu.gameObject.SetActive(false);
                    brochureOn = true;
                    if(playVid.activeSelf)
                    {
                        playVid.SetActive(false);
                    }
                        pageBG.enabled = false;
                        brochure.SetActive(true);
                        infoVR.SetActive(false);
                    //}
                    //else
                    //{
                    //    brochureOn = false;
                    //    pageBG.enabled = true;
                    //    brochure.SetActive(false);
                    //    infoVR.SetActive(true);
                    //}
                    break;

                case EnumCollections.ClickableButtons.PlayVid:
                    Handheld.PlayFullScreenMovie("VRChennai.mp4", Color.black, FullScreenMovieControlMode.Full, FullScreenMovieScalingMode.AspectFit);
                    break;
                default:
                    break;
            }
        }
	}

    void DisplayPic(int imageNum)
    {

        gallerybg.sprite = pics[imageNum];
    }

    public void OnCatchmentBnSelected(int button)
    {
        var Bn = (EnumCollections.CatchmentBns)button;
        blocker.SetActive(true);
        var root = blocker.transform.parent;
        var superPrimary = root.Find("SuperPrimary");
        var primary = root.Find("Primary");
        var secondary = root.Find("Secondary");

        switch (Bn)
        {
            case EnumCollections.CatchmentBns.SuperPrimary:
                secondary.SetSiblingIndex(0);
                primary.SetSiblingIndex(1);
                blocker.transform.SetSiblingIndex(2);
                superPrimary.transform.SetSiblingIndex(3);
                break;
            case EnumCollections.CatchmentBns.Primary:
                secondary.SetSiblingIndex(0);
                superPrimary.SetSiblingIndex(1);
                blocker.transform.SetSiblingIndex(2);
                primary.transform.SetSiblingIndex(3);
                break;
            case EnumCollections.CatchmentBns.Secondary:
                primary.SetSiblingIndex(0);
                superPrimary.SetSiblingIndex(1);
                blocker.transform.SetSiblingIndex(2);
                secondary.transform.SetSiblingIndex(3);
                break;
            case EnumCollections.CatchmentBns.None:
                break;
            default:
                break;
        }
    }

    void OnSubMenuSelected(int subMenuNum)
    {
        Image currentBnImage = null;
        if (subMenu.transform.childCount > 0)
        {
            currentBnImage = subMenu.transform.GetChild(subMenuNum).GetChild(0).GetComponent<Image>();
            if (prevBnImage == null)
            {
                prevBnImage = currentBnImage;
                prevBnColour = currentBnImage.color;
                currentBnImage.color = Color.white;
                currentBnImage.sprite = selectedBn;
            }
            else if(prevBnImage == currentBnImage)
            {
                currentBnImage.color = prevBnColour;
                currentBnImage.sprite = baseSprite;
                prevBnImage = null;
            }
            else
            {
                prevBnImage.color = prevBnColour;
                prevBnImage.sprite = baseSprite;
                prevBnColour = currentBnImage.color;
                currentBnImage.color = Color.white;
                currentBnImage.sprite = selectedBn;
                prevBnImage = currentBnImage;
            }
            
        }
        
        switch (currentPageBn)
        {
            case EnumCollections.ClickableButtons.Home:
                pageBG.enabled = false;
                mapBG.enabled = false;
				compass.SetActive (true);

                ResetAccessPoints();
                if (subMenuNum == 0)
                {
                    accessibilityLegends.SetActive(false);
                    if (!accessPoints.activeSelf)
                    {
                        accessPoints.SetActive(true);
                    }
                    else
                    {
                        accessPoints.SetActive(false);
                    }
                    if (accessibilityPoints.activeSelf)
                    {
                        accessibilityPoints.SetActive(false);
                    }
                }
                else
                {
                    if(accessPoints.activeSelf)
                    {
                        accessPoints.SetActive(false);
                    }

                    if (!accessibilityPoints.activeSelf)
                    {
                        accessibilityPoints.SetActive(true);
                    }
                    else
                    {
                        accessibilityPoints.SetActive(false);
                    }

					if(accessibilityLegends.activeSelf)
					{
						accessibilityLegends.SetActive(false);	
					}
					else
					{
						accessibilityLegends.SetActive(true);	
					}
                    
                }
                if (stackRoot.activeSelf)
                {
                    AnimationHandler.Singleton.ResetActiveStack();
                    AnimationHandler.Singleton.ResetScreens();
                }
                if (!MainCam.enabled)
                {
                    MainCam.enabled = true;
                    mapCam.clearFlags = CameraClearFlags.Depth;
                }
                break;
            case EnumCollections.ClickableButtons.Floorplans:
                pageBG.enabled = false;
                if (MainCam.enabled)
                {
                    MainCam.enabled = false;
                    mapCam.clearFlags = CameraClearFlags.SolidColor;
                    
                }
                mapCam.transform.position = mapCamStartPos;
                mapCam.orthographicSize = 100;
                if (subMenuNum == 5)
                {
                    ruler.enabled = false;
                    mapBG.enabled = false;
                    stackRoot.SetActive(true);
                    AnimationHandler.Singleton.SetScreens();                    
                }
                else
                {
                    mapBG.enabled = true;
                    if (stackRoot.activeSelf)
                    {
                        AnimationHandler.Singleton.ResetActiveStack();
                        AnimationHandler.Singleton.ResetScreens();
                    }
                    ruler.enabled = true;
                    mapBG.sprite = floorPlanBGs[subMenuNum];                    
                }
                break;
            case EnumCollections.ClickableButtons.Catchment:
                infoToggleBn.SetActive(true);
                catchmentZoom.enabled = true;
                mapCam.orthographicSize = 100;
                pageBG.enabled = false;
                mapBG.enabled = false;
                ruler.enabled = false;
                blocker.SetActive(false);
                var root = blocker.transform.parent;
                var superPrimary = root.Find("SuperPrimary");
                var primary = root.Find("Primary");
                var secondary = root.Find("Secondary");

                secondary.transform.SetSiblingIndex(0);
                primary.SetSiblingIndex(1);
                superPrimary.SetSiblingIndex(2);
                mapCam.transform.position = mapCamStartPos;

                for (int i = 0; i < catchmentOptions.Count; i++)
                {
                    infoPanel.transform.GetChild(i).gameObject.SetActive(false);
                    catchmentRoot.transform.GetChild(i).gameObject.SetActive(false);
                }

                if (stackRoot.activeSelf)
                {
                    AnimationHandler.Singleton.ResetActiveStack();
                    AnimationHandler.Singleton.ResetScreens();
                }
                catchmentRoot.transform.GetChild(subMenuNum).gameObject.SetActive(true);
                infoPanel.transform.GetChild(subMenuNum).gameObject.SetActive(true);
                if (MainCam.enabled)
                {
                    MainCam.enabled = false;
                    mapCam.clearFlags = CameraClearFlags.SolidColor;
                }
                break;
            case EnumCollections.ClickableButtons.Connectivity:
                pageBG.enabled = false;
                mapBG.enabled = false;
				compass.SetActive (false);
				connectivityLegends.SetActive (true);
                if (stackRoot.activeSelf)
                {
                    AnimationHandler.Singleton.ResetActiveStack();
                    AnimationHandler.Singleton.ResetScreens();
                }
                if (!MainCam.enabled)
                {
                    MainCam.enabled = true;
                    mapCam.clearFlags = CameraClearFlags.Depth;
                }   
                break;
            default:
                break;
        }
        
        
    }

	public void ClosePopUp()
	{
		popUp.SetActive (false);
	}

	public void SetPopUp(string popUpText)
	{
		var text = popUp.GetComponentInChildren <Text> ();
		text.text = popUpText;
		popUpTarget = currentPopUpBn.GetComponent <RectTransform> ();
		popUpWorldPos = popUpTarget.position;
		popUp.SetActive (true);
		var temp = MainCam.WorldToScreenPoint (popUpWorldPos);
		popUpRect.anchoredPosition = new Vector2(temp.x - (popUpCanvas.referenceResolution.x * 0.5f), temp.y - (popUpCanvas.referenceResolution.y * 0.5f));
	}

	public void SetPopUpOption(GameObject currentPopUp)
	{
		currentPopUpBn = currentPopUp;
	}
    
    void OnAmenitySelected(EnumCollections.Amenities amenity)
    {
        ResetAccessPoints();
        if (amenity == EnumCollections.Amenities.Close)
        {
            
            return;
        }
        frame3DTarget = accessPoints.transform.GetChild((int)amenity).GetChild(0).GetComponent<RectTransform>();
		framePos = frame3DTarget.position;
        frame.SetActive(true);
		var temp = MainCam.WorldToScreenPoint(framePos);
		frameRect.anchoredPosition = new Vector2(temp.x - (popUpCanvas.referenceResolution.x * 0.5f), temp.y - (popUpCanvas.referenceResolution.y * 0.5f));
        amenityImg.sprite = amenityPics[(int)amenity];
        amenityText.text = amenityOptions[(int)amenity];
        //.gameObject.SetActive(false);
    }

    void ResetAccessPoints()
    {
        foreach (Transform t in accessPoints.transform)
        {
            if (!t.GetChild(0).gameObject.activeSelf)
            {
                t.GetChild(0).gameObject.SetActive(true);
            }
            frame.SetActive(false);
        }
    }

    void PopulateSubmenu(EnumCollections.ClickableButtons clickedBn)
    {
        List<InputHandler> inputList = new List<InputHandler>();
        subMenu.DestroyChildren();
        AnimationHandler.Singleton.stackMenu.Clear();
        switch (clickedBn)
        {
            case EnumCollections.ClickableButtons.Home:
                inputList = AddMenuItems(homeOptions, 0);
                foreach (InputHandler ip in inputList)
                {
                    ip.home = (EnumCollections.HomeSubMenu)inputList.IndexOf(ip);
                }
                break;
            case EnumCollections.ClickableButtons.Catchment:
                inputList = AddMenuItems(catchmentOptions, 1);
                foreach (InputHandler ip in inputList)
                {
                    ip.catchment = (EnumCollections.CatchmentSubMenu)inputList.IndexOf(ip);
                }
                break;
            case EnumCollections.ClickableButtons.Floorplans:
                inputList = AddMenuItems(floorOptions, 2);
                inputList.Reverse();
                foreach (InputHandler ip in inputList)
                {
                    ip.floorplans = (EnumCollections.FloorplansSubMenu)inputList.IndexOf(ip);
                }
                break;
            default:
                break;
        }
        foreach (Transform child in subMenu.transform)
        {
            AnimationHandler.Singleton.stackMenu.Add(child.GetChild(0).GetComponent<Animator>());
        }
        AnimationHandler.Singleton.stackMenu.Reverse();
        StartCoroutine(AnimationHandler.Singleton.PlayAnim(AnimationHandler.Singleton.stackMenu, true));
    }

    List<InputHandler> AddMenuItems(List<string> options, int itemListIndex)
    {
        var item = subMenuItemsList[itemListIndex];

        var itemInfoArray = item.Split('|');

        var itemSizeInfo = itemInfoArray[0].Split('.');
        var itemColourInfo = itemInfoArray[1].Split('.');

        List<InputHandler> inputList = new List<InputHandler>();

        for (int i = 0; i < options.Count; i++)
        {
            var sizeIndex = int.Parse(itemSizeInfo[i]);
            var colourIndex = int.Parse(itemColourInfo[i]);

            var option = Instantiate(subMenuSizes[sizeIndex]).transform.GetChild(0);
            option.GetComponent<Image>().color = subMenuColours[colourIndex];
            option.GetChild(0).GetComponent<Text>().text = options[i];
            inputList.Add(option.GetComponent<InputHandler>());
            option.parent.SetParent(subMenu.transform, false);
        }
        return inputList;
    }
    
    void ToggleMenu()
    {
        toggleMenu = !toggleMenu;
        if(!toggleMenu)
        {
            menuToggle.sprite = hideMenu;
        }
        else
        {
            menuToggle.sprite = showMenu;
        }
        taskAnim.SetBool("ToggleVisibility", toggleMenu);
    }

    void ToggleInfo()
    {
        toggleInfo = !toggleInfo;
        infoAnim.SetBool("ToggleVisibility", toggleInfo);
    }

    public void SetRulerStatus(bool status)
    {
        ruler.enabled = status;
    }

    public bool GetRulerStatus()
    {
        return ruler.enabled;
    }
}
