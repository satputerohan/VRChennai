﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class CustomSceneManager : MonoBehaviour {

    bool animFinished, loadedSceneinBG;
    AsyncOperation asyncOp;

    void Start () {
        asyncOp = SceneManager.LoadSceneAsync(2, LoadSceneMode.Additive);        
	}
	
	void OnEnable()
    {
        EventDispatcher.Singleton.onAnimFinished += OnAnimFinished;
    }

    void OnDisable()
    {
        EventDispatcher.Singleton.onAnimFinished -= OnAnimFinished;
    }

    void OnAnimFinished()
    {
        animFinished = true;
    }

    void Update()
    {
        if (asyncOp.isDone && !loadedSceneinBG)
        {
            loadedSceneinBG = true;
        }

        if (animFinished && loadedSceneinBG)
        {
            SceneManager.UnloadScene("Splash");
        }
    }
}
