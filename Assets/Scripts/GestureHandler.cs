﻿using UnityEngine;
using UnityEngine.EventSystems;

public class GestureHandler : MonoBehaviour//, IPointerDownHandler, IPointerUpHandler, IEndDragHandler {
{ 
    Vector2 startPos, endPos;
    [SerializeField] float swipeMinDistance;

    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            startPos = Input.mousePosition;
        }

        if(Input.GetMouseButtonUp(0))
        {
            endPos = Input.mousePosition;

            if (Mathf.Abs(endPos.x - startPos.x) > swipeMinDistance)
            {
                if (startPos.x > endPos.x)
                {
                    //LeftSwipe
                    EventDispatcher.Singleton.ButtonClicked(EnumCollections.ClickableButtons.GalleryNext);
                }
                else if (startPos.x < endPos.x)
                {
                    //RightSwipe
                    EventDispatcher.Singleton.ButtonClicked(EnumCollections.ClickableButtons.GalleryPrev);
                }
            }
        }
    }

    //public void OnPointerDown(PointerEventData pData)
    //{
    //    Debug.Log("Down");
    //    startPos = pData.position;
    //}

    //public void OnPointerUp(PointerEventData pData)
    //{
    //    Debug.Log("Up");
    //    endPos = pData.position;

    //    if (Mathf.Abs(endPos.x - startPos.x) > swipeMinDistance)
    //    {
    //        if (startPos.x > endPos.x)
    //        {
    //            //LeftSwipe
    //            EventDispatcher.Singleton.ButtonClicked(EnumCollections.ClickableButtons.GalleryNext);
    //        }
    //        else if (startPos.x < endPos.x)
    //        {
    //            //RightSwipe
    //            EventDispatcher.Singleton.ButtonClicked(EnumCollections.ClickableButtons.GalleryPrev);
    //        }
    //    }
    //}

    //public void OnEndDrag(PointerEventData pData)
    //{
    //    Debug.Log("Drag End");
    //    endPos = pData.position;

    //    if (Mathf.Abs(endPos.x - startPos.x) > swipeMinDistance)
    //    {
    //        if (startPos.x > endPos.x)
    //        {
    //            //LeftSwipe
    //            EventDispatcher.Singleton.ButtonClicked(EnumCollections.ClickableButtons.GalleryNext);
    //        }
    //        else if (startPos.x < endPos.x)
    //        {
    //            //RightSwipe
    //            EventDispatcher.Singleton.ButtonClicked(EnumCollections.ClickableButtons.GalleryPrev);
    //        }
    //    }
    //}
}
