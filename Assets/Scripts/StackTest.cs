﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StackTest : MonoBehaviour {

    Vector3 initPos, endPos, endScale;
    Quaternion startRot, endRot;
    Ray ray;
    RaycastHit hitInfo;
    int focusedOn = -1;
    [SerializeField] float offset = 1.25f;
    private bool hasMoved;
    
    void Start () {
        startRot = Quaternion.Euler(Vector3.right * -20f);
        int index = 0;
        foreach (Transform child in transform)
        {
            UIManager.instance.startPosList[index] = child.localPosition;
            index++;
        }        
        endPos = new Vector3(0.8f, -0.5f, -1.5f);
        endScale = transform.localScale * 1.5f;
        endRot = Quaternion.Euler(Vector3.right * -30f);
	}

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && UIManager.instance.isStackVisible)
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hitInfo))
            {
                var hitTrans = hitInfo.transform.parent;
                var hitIndex = hitTrans.GetSiblingIndex();

                if (focusedOn != hitIndex)
                {
                    foreach (Transform child in transform)
                    {
                        var childIndex = child.GetSiblingIndex();

                        if (hitIndex > childIndex)
                        {
                            StartCoroutine(MoveUp(child, childIndex, false));
                        }
                        else if (hitIndex < childIndex)
                        {
                            StartCoroutine(MoveDown(child, childIndex, false));
                        }
                        else
                        {
                            focusedOn = childIndex;
                            StartCoroutine(Focus(child));
                        }
                    }
                    
                }
                else
                {
                    foreach (Transform child in transform)
                    {
                        var childIndex = child.GetSiblingIndex();
                        if (hitIndex < childIndex)
                        {
                            StartCoroutine(MoveUp(child, childIndex, true));
                        }
                        else if (hitIndex > childIndex)
                        {
                            StartCoroutine(MoveDown(child, childIndex, true));
                        }
                        else
                        {
                            focusedOn = -1;
                            StartCoroutine(Unfocus(child));
                        }
                    }
                    
                }
            }
        }
    }
	
    IEnumerator MoveUp(Transform moveTrans, int transIndex, bool isReturning)
    {
        var rate = 5f;
        var sec = 0f;
        var upPos = Vector3.zero;
        var curPos = moveTrans.localPosition;
        var curRot = moveTrans.rotation;
        var curScale = moveTrans.localScale;
        if (isReturning)
        {
            upPos = UIManager.instance.startPosList[transIndex];
        }
        else
        {
            upPos = UIManager.instance.startPosList[transIndex] + Vector3.up * offset;
        }
        while (sec <= 1f)
        {
            sec += Time.deltaTime * rate;
            moveTrans.localPosition = Vector3.Lerp(curPos, upPos, sec);
            moveTrans.rotation = Quaternion.Lerp(curRot, startRot, sec);
            moveTrans.localScale = Vector3.Lerp(curScale, Vector3.one, sec);
            yield return null;
        }
    }

    IEnumerator MoveDown(Transform moveTrans, int transIndex, bool isReturning)
    {
        var rate = 5f;
        var sec = 0f;
        var curPos = moveTrans.localPosition;
        var curRot = moveTrans.rotation;
        var curScale = moveTrans.localScale;
        var downPos = Vector3.zero;
        if (isReturning)
        {
            downPos = UIManager.instance.startPosList[transIndex];
        }
        else
        {
            downPos = UIManager.instance.startPosList[transIndex] - Vector3.up * offset;
        }

        while (sec <= 1f)
        {
            sec += Time.deltaTime * rate;
            moveTrans.localPosition = Vector3.Lerp(curPos, downPos, sec);
            moveTrans.rotation = Quaternion.Lerp(curRot, startRot, sec);
            moveTrans.localScale = Vector3.Lerp(curScale, Vector3.one, sec);
            yield return null;
        }
    }

    IEnumerator Focus (Transform focusOn) {
        var rate = 5f;
        var sec = 0f;
        var startPos = focusOn.localPosition;
        while (sec <= 1f)
        {
            sec += Time.deltaTime * rate;
            focusOn.localPosition = Vector3.Lerp(startPos, endPos, sec);
            focusOn.rotation = Quaternion.Lerp(startRot, endRot, sec);
            focusOn.localScale = Vector3.Lerp(Vector3.one, endScale, sec);
            yield return null;
        }
	}

    IEnumerator Unfocus(Transform unfocusFrom)
    {
        var rate = 5f;
        var sec = 0f;
        var startPos = UIManager.instance.startPosList[unfocusFrom.GetSiblingIndex()];

        while (sec <= 1f)
        {
            sec += Time.deltaTime * rate;
            unfocusFrom.localPosition = Vector3.Lerp(endPos, startPos, sec);
            unfocusFrom.rotation = Quaternion.Lerp(endRot, startRot, sec);
            unfocusFrom.localScale = Vector3.Lerp(endScale, Vector3.one, sec);
            yield return null;
        }
    }
}
