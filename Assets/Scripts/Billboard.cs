﻿using UnityEngine;
using System.Collections;

//[ExecuteInEditMode]
public class Billboard : MonoBehaviour {


    void Update()
    {
        if (Camera.main.enabled)
        {
            transform.LookAt(transform.position + Camera.main.transform.rotation * Vector3.forward,
                Camera.main.transform.rotation * Vector3.up);
        }
    }
}
