﻿using UnityEngine;
using System.Collections.Generic;

public static class ExtensionMethods {

    public static void DestroyChildren(this GameObject go)
    {

        List<GameObject> children = new List<GameObject>();
        foreach (Transform tran in go.transform)
        {
            children.Add(tran.gameObject);
        }
        children.ForEach(child => GameObject.Destroy(child));
        go.transform.DetachChildren();
    }

    public static bool AlmostEquals(this float num, float to, float offset)
    {
        if(Mathf.Abs(num - to) > offset)
        {
            return false;
        }
        else if(Mathf.Abs(num - to) < offset)
        {
            return true;
        }
        else
        {
            return true;
        }
    }
}
