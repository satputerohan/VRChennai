﻿using UnityEngine;
using System.Collections;

public class VanishingBuildings : MonoBehaviour {

    MeshRenderer[] meshes;
    
    void Start()
    {
        meshes = transform.parent.gameObject.GetComponentsInChildren<MeshRenderer>();
    }

	void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("MainCamera"))
        {
            foreach (MeshRenderer mr in meshes)
            {
                mr.enabled = false;
                UIManager.instance.towerCollidersActive.Add(mr.gameObject);
            }
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.CompareTag("MainCamera") && UIManager.instance.towerCollidersActive.Count > 0)
        {
            foreach (MeshRenderer mr in meshes)
            {
                mr.enabled = true;
                UIManager.instance.towerCollidersActive.Remove(mr.gameObject);
            }
        }
    }
}
