﻿using UnityEngine;
using System.Collections;

public class AnimFinishEvent : MonoBehaviour {

	public void AnimFinish()
    {
        transform.parent.GetComponent<LoadSceneAsync>().animFinished = true;
    }
}
