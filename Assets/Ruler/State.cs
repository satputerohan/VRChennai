﻿
	using UnityEngine;
	using System;
	using System.Collections.Generic;
	
	public class State {

		static State instance;

		public static State Instance {
			get {
				if( instance == null ) {
					instance = new State(); 
				}
				return instance;
			}
		}

		public Context context;
		public bool displayCoords;
		public bool preferColliders;

		private State() {
			LoadContext();
		}

		public void Save() {
			SaveContext();
		}

		#region Context
		void SaveContext() {
			PlayerPrefs.SetInt( prefContextInstanceId, context.instanceId );
		}

		void LoadContext() {
			int instanceId = PlayerPrefs.GetInt( prefContextInstanceId, 0 );

			if( instanceId == 0 ) {
				context = new Context( ContextType.EditorScene, null );
			}
			else {
				object[] objs = GameObject.FindObjectsOfType( typeof(GameObject) );
				foreach( object obj in objs ) {
					GameObject gameObj = (GameObject)obj;
					if( gameObj.GetInstanceID() == instanceId ) {
						if( gameObj.GetComponent<Canvas>() != null ) {
							context = new Context( ContextType.Canvas, gameObj );
							break;
						}
					}
				}
			}

			// catch all
			if( context == null ) {
				context = new Context( ContextType.EditorScene, null );
			}
		}
		#endregion



		const string prefContextInstanceId 	= "R2DPREF_ContextInstanceId";
	}
