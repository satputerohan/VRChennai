﻿
	using UnityEngine;

	public class ContextInfo {
		static ContextInfo instance;
		
		public static ContextInfo Instance {
			get {
				if( instance == null ) {
					instance = new ContextInfo();
				}
				return instance;
			}
		}

		public Frame	viewPixelFrame;
		public Frame	viewUnitFrame;
		public Vector2	gridPixelSquareSize;
		public float	editorCameraSize;
		public Vector3	origin;
		public Vector3 	scale;
		public Camera cam;

		private ContextInfo( ) { 
			viewPixelFrame = new Frame();
			viewUnitFrame = new Frame();
		}

		public bool LoadContextInfo() {
			State state = State.Instance;
			//cam = Camera.main;
			editorCameraSize = cam.orthographicSize;
			gridPixelSquareSize = cam.WorldToScreenPoint( new Vector3(1, 1) ) - cam.WorldToScreenPoint( new Vector3(0, 0) );
			scale = new Vector3(0.89f, 0.89f, 1);
			origin = Vector3.zero;
			
			Context context = state.context;
			
			if( context.type == ContextType.Canvas ) {
				if( context.gameObject == null ) {
					return false;
				}

				scale = context.gameObject.transform.lossyScale;
				origin = context.gameObject.GetComponent<RectTransform>().position;
				
				if( scale.x <= 0.001f || scale.y <= 0.001f ) {
					return false;
				}
			}
			else if( context.type == ContextType.NGUI ) {
				if( context.gameObject == null ) {
					return false;
				}

				scale = context.gameObject.transform.lossyScale;
				origin = context.gameObject.GetComponent<Transform>().position;

			}
			
			LoadPixelFrame();
			LoadWorldUnits();

			return true;
		}

		void LoadPixelFrame() {
			viewPixelFrame.topLeft = cam.pixelRect.min;
        
			viewPixelFrame.botLeft = new Vector2( viewPixelFrame.topLeft.x, viewPixelFrame.topLeft.y + cam.pixelRect.height );
			//viewPixelFrame.botLeft.y--;
			
			viewPixelFrame.botRight	= cam.pixelRect.max;
			//viewPixelFrame.botRight.x--;
			//viewPixelFrame.botRight.y--;
			viewPixelFrame.topRight	= new Vector2( viewPixelFrame.botRight.x, viewPixelFrame.topLeft.y );
			
			viewPixelFrame.width = viewPixelFrame.topRight.x - viewPixelFrame.topLeft.x;
			viewPixelFrame.height = viewPixelFrame.botRight.y - viewPixelFrame.topRight.y;
		}

		void LoadWorldUnits() {
			viewUnitFrame.botLeft	= cam.ScreenToWorldPoint( viewPixelFrame.topLeft );
			viewUnitFrame.topLeft	= cam.ScreenToWorldPoint( viewPixelFrame.botLeft );
            viewUnitFrame.topRight	= cam.ScreenToWorldPoint( viewPixelFrame.topRight );
			viewUnitFrame.botRight	= cam.ScreenToWorldPoint( viewPixelFrame.botRight );
		}

	}