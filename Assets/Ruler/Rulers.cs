﻿
using UnityEngine;

public class Rulers {
	static Rulers instance;
		
	public static Rulers Instance {
		get {
			if( instance == null ) {
				instance = new Rulers();
			}
			return instance;
		}
	}

	Scale[] scaleTable;
	Drawing drawing;
	Frame viewPixelFrame;
	Frame viewUnitFrame;
	RulerResources resources;
	ContextInfo contextInfo;
	Utils utils;
    public static int resScaleFactor = (Screen.dpi > 250) ? 2 : 1;

    int vLineLabelXOffset = 4 * resScaleFactor;
    int vLineLabelYOffset = 3 * resScaleFactor;
    int hLineLabelXOffset = 2 * resScaleFactor;
    int hLineLabelYOffset = 4 * resScaleFactor;

    int cornerSize = 20 * resScaleFactor;
    int stripBgSize = 18 * resScaleFactor;
    int stripHiSize = 17 * resScaleFactor;
    int stripMidSize = 6 * resScaleFactor;
    int stripMidOffset = 11 * resScaleFactor;
    int stripLowSize = 4 * resScaleFactor;
    int stripLowOffset = 13 * resScaleFactor;

    float scaleShiftThreshold = 22f * resScaleFactor;

    private Rulers() {
		contextInfo 	= ContextInfo.Instance;
		drawing 		= Drawing.Instance;
		viewUnitFrame 	= contextInfo.viewUnitFrame;
		viewPixelFrame 	= contextInfo.viewPixelFrame;
		resources 		= RulerResources.Instance;
		utils			= Utils.Instance;

		InitScaleTable();
	}

	public void DrawRulers() {
		DrawHRulers();
		DrawVRulers();
		DrawCorner();
	}

	void DrawHRulers() {
		drawing.DrawTexture( resources.rulerHBg, 0, (int)viewPixelFrame.height - resources.rulerHBg.height * resScaleFactor, (int)viewPixelFrame.width, stripBgSize);

		float camSize = contextInfo.editorCameraSize / contextInfo.scale.x;
		float rawSegmentUnitWidth = scaleTable[ GetScaleKey( camSize ) ].cap;
		float segmentUnitWidth = rawSegmentUnitWidth * contextInfo.scale.x;

		float startingLeftUnit = viewUnitFrame.topLeft.x - segmentUnitWidth;
		startingLeftUnit = startingLeftUnit - (startingLeftUnit % segmentUnitWidth);
			
		float endingRightUnit = viewUnitFrame.botRight.x + segmentUnitWidth;
		endingRightUnit = endingRightUnit - (endingRightUnit % segmentUnitWidth);

		startingLeftUnit += contextInfo.origin.x % segmentUnitWidth;;

		float runningUnit = startingLeftUnit; 
		float runningPixel = utils.GetWorldToScreenPixelCoord( new Vector3( runningUnit, viewUnitFrame.topLeft.y ) ).x;
		float runningPixelCap = utils.GetWorldToScreenPixelCoord( new Vector3( runningUnit + segmentUnitWidth, viewUnitFrame.topLeft.y ) ).x;
		float innerSlicesDist = ( runningPixelCap - runningPixel ) / 4f;

		while ( runningUnit < endingRightUnit ) {
			// Draw main coord
			DrawVLine( runningPixel, (int)viewPixelFrame.height - stripHiSize, stripHiSize, resources.rulerLinePixel );
			string virtualXCoord = FormatNumber( utils.GetWorldToContextX( runningUnit ), camSize );
			DrawVLineLabel( virtualXCoord, runningPixel + vLineLabelXOffset, (int)viewPixelFrame.height - stripHiSize + vLineLabelYOffset );
			
			// Draw slices
			runningPixel += innerSlicesDist;
			DrawVLine( runningPixel, (int)viewPixelFrame.height - stripHiSize + stripLowOffset, stripLowSize, resources.rulerLinePixel );
			runningPixel += innerSlicesDist;
			DrawVLine( runningPixel, (int)viewPixelFrame.height - stripHiSize + stripMidOffset, stripMidSize, resources.rulerLinePixel );
			runningPixel += innerSlicesDist;
			DrawVLine( runningPixel, (int)viewPixelFrame.height - stripHiSize + stripLowOffset, stripLowSize, resources.rulerLinePixel );

			// Move to next segment
			runningUnit += segmentUnitWidth;
			runningPixel = utils.GetWorldToScreenPixelCoord( new Vector3( runningUnit, viewUnitFrame.topLeft.y ) ).x;
		}
	}

	void DrawVRulers() {
		drawing.DrawTexture( resources.rulerVBg, 0, 0, stripBgSize, (int)viewPixelFrame.height );

		float camSize = contextInfo.editorCameraSize / contextInfo.scale.y;
		float rawSegmentUnitHeight = scaleTable[ GetScaleKey( camSize ) ].cap;
		float segmentUnitHeight = rawSegmentUnitHeight * contextInfo.scale.y;
			
		float startingBotUnit = viewUnitFrame.botLeft.y - segmentUnitHeight;
		startingBotUnit = startingBotUnit - (startingBotUnit % segmentUnitHeight);

		float endingTopUnit = viewUnitFrame.topLeft.y + segmentUnitHeight;
		endingTopUnit = endingTopUnit - (endingTopUnit % segmentUnitHeight);

		startingBotUnit += contextInfo.origin.y % segmentUnitHeight;;

		float runningUnit = startingBotUnit; 
		float runningPixel = viewPixelFrame.height - utils.GetWorldToScreenPixelCoord( new Vector3( viewUnitFrame.topLeft.x, runningUnit ) ).y; 
		float runningPixelCap = viewPixelFrame.height - utils.GetWorldToScreenPixelCoord( new Vector3( viewUnitFrame.topLeft.x, runningUnit + segmentUnitHeight ) ).y; 
		float innerSlicesDist = ( runningPixelCap - runningPixel ) / 4f; 

		while ( runningUnit < endingTopUnit ) {
			// Draw main coord
			DrawHLine( viewPixelFrame.topLeft.x, runningPixel, stripHiSize, resources.rulerLinePixel );
			string virtualYCoord = FormatNumber( utils.GetWorldToContextY( runningUnit ), camSize );
			DrawHLineLabel( virtualYCoord.ToString(), viewPixelFrame.topLeft.x + hLineLabelXOffset, runningPixel + hLineLabelYOffset );
			
			// Draw slices
			runningPixel += innerSlicesDist;
			DrawHLine( viewPixelFrame.topLeft.x + stripLowOffset, runningPixel, stripLowSize, resources.rulerLinePixel );
			runningPixel += innerSlicesDist;
			DrawHLine( viewPixelFrame.topLeft.x + stripMidOffset, runningPixel, stripMidSize, resources.rulerLinePixel );
			runningPixel += innerSlicesDist;
			DrawHLine( viewPixelFrame.topLeft.x + stripLowOffset, runningPixel, stripLowSize, resources.rulerLinePixel );

			// Move to next segment
			runningUnit += segmentUnitHeight;
			runningPixel =  viewPixelFrame.height - utils.GetWorldToScreenPixelCoord( new Vector3( viewUnitFrame.topLeft.x, runningUnit ) ).y;

		}
	}

	string FormatNumber( float num, float camSize ) {
		if( camSize > 25000 ) {
			return "";
		}

		num = Mathf.Round( num * 2f ) / 2f;
		string prefix = "";
		if ( num < 0 ) {
			prefix = "-";
		}
		num = Mathf.Abs( num );

		if (num >= 1000000)
			return prefix + (num / 1000000).ToString("0.#") + "M";
			
		if (num >= 100000)
			return prefix + (num / 1000).ToString("#0K");
			
		if (num >= 10000)
			return prefix + (num / 1000).ToString("0.#") + "K";

			
		return prefix + num.ToString();
	}
		
	void DrawVLine( float xPos, float yPos, int height, Texture2D texture ) {
		drawing.DrawTexture( 	texture, 
			                    Mathf.FloorToInt( xPos ),
			                    Mathf.FloorToInt( yPos ),
			                    1,
			                    height );
	}

	void DrawVLineLabel( string text, float xPos, float yPos ) {
		drawing.DrawPanelLabel( text, 
			                    Mathf.FloorToInt( xPos ), 
			                    Mathf.FloorToInt( yPos ) );
	}

	void DrawHLine( float xPos, float yPos, int width, Texture2D texture ) {

		drawing.DrawTexture( 	texture, 
			                    Mathf.CeilToInt( xPos ),
			                    Mathf.CeilToInt( yPos ),
			                    width,
			                    1 );
	}
		
	void DrawHLineLabel( string text, float xPos, float yPos ) {
		drawing.DrawVLabel( text, 
			                Mathf.CeilToInt( xPos ),
			                Mathf.CeilToInt( yPos ) );
	}


	void DrawCorner() {
		Texture2D corner = resources.corner;
		drawing.DrawTexture( corner, 0, (int)viewPixelFrame.height - resources.corner.height * resScaleFactor, 
                            corner.width * resScaleFactor, corner.height * resScaleFactor);
	}
    
	struct Scale {
		public float cap;
			
		public Scale( float cap ) {
			this.cap = cap;
		}
	};

	int GetScaleKey( float cameraSize ) {
		int key;
		if ( cameraSize < 1.5 )			key = 1;
		else if ( cameraSize < 5 )		key = 1;
		else if ( cameraSize < 30 ) 	key = 2;
		else if ( cameraSize < 55 ) 	key = 3;
		else if ( cameraSize < 250 ) 	key = 4;
		else if ( cameraSize < 500 ) 	key = 5;
		else if ( cameraSize < 2000 ) 	key = 6;
		else if ( cameraSize < 5000 ) 	key = 7;
		else 							key = 8;
		return key;
	}

	void InitScaleTable() {
		scaleTable = new Scale[9];
		scaleTable[ 0 ] = new Scale( 0.5f	);
		scaleTable[ 1 ] = new Scale( 1		);
		scaleTable[ 2 ] = new Scale( 5		);
		scaleTable[ 3 ] = new Scale( 10		);
		scaleTable[ 4 ] = new Scale( 50		);
		scaleTable[ 5 ] = new Scale( 100	);
		scaleTable[ 6 ] = new Scale( 500	);
		scaleTable[ 7 ] = new Scale( 1000	);
		scaleTable[ 8 ] = new Scale( 5000	);
	}

	void DrawPixelTest() {
		DrawPixel( viewPixelFrame.topLeft ); 
		DrawPixel( viewPixelFrame.botLeft );
		DrawPixel( viewPixelFrame.topRight ); 
		DrawPixel( viewPixelFrame.botRight ); 
	}
		
	void DrawPixel( Vector2 coords ) {
		int x = (int)coords.x;
		int y = (int)coords.y;
		drawing.DrawTexture( resources.pixel, x, y, 1, 1 );
			
	}
        
	
}