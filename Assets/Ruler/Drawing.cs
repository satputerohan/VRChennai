﻿
using UnityEngine;
using System.Text;

public class Drawing {

	static Drawing instance;
		
	public static Drawing Instance {
		get {
			if( instance == null ) {
				instance = new Drawing(); 
			}
			return instance;
		}
	}

	private Drawing() {
		LoadFontStyles();
	}


	#region Scene View Helpers
	public void DrawTexture( Texture2D texture, int x, int y, int width, int height ) {
		GUI.DrawTexture( new Rect(x, y, width, height ),
			                texture, 
			                ScaleMode.StretchToFill,
			                true, 
			                0	);
	}

	public void DrawFloatTexture( Texture2D texture, float x, float y, float width, float height ) {
		GUI.DrawTexture( new Rect(x, y, width, height ),
			            texture, 
			            ScaleMode.StretchToFill,
			            false, 
			            0	);
	}

	public void DrawSimpleTexture( Texture2D texture, Vector2 location ) {
		location.x -= texture.width / 2f;
		location.y -= texture.height / 2f;
		GUI.DrawTexture( new Rect( location, new Vector2( texture.width, texture.height ) ), texture );
	}
			
	public void DrawLine( Texture2D texture, Vector2 start, Vector2 end ) {
		DrawLineExec( (int)start.x, (int)start.y, (int)end.x, (int)end.y, texture );
	}

	public void DrawLineExec( int x,int y,int x2, int y2, Texture2D texture ) {
		int w = x2 - x;
		int h = y2 - y;
		int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
		if ( w < 0 ) dx1 = -1 ; else if ( w > 0 ) dx1 = 1;
		if ( h < 0 ) dy1 = -1 ; else if ( h > 0 ) dy1 = 1;
		if ( w < 0 ) dx2 = -1 ; else if ( w > 0 ) dx2 = 1;
		int longest = Mathf.Abs( w );
		int shortest = Mathf.Abs( h );
		if ( !( longest > shortest ) ) {
			longest = Mathf.Abs( h );
			shortest = Mathf.Abs( w );
			if ( h < 0 ) dy2 = -1 ; else if ( h > 0 ) dy2 = 1;
			dx2 = 0;            
		}
		int numerator = longest >> 1;
		for ( int i = 0; i <= longest; i++ ) {
			GUI.DrawTexture( new Rect( x, y, 1, 1 ), texture );
			numerator += shortest;
			if ( !( numerator < longest ) ) {
				numerator -= longest;
				x += dx1;
				y += dy1;
			} else {
				x += dx2;
				y += dy2;
			}
		}
	}


	public void DrawPanelLabel( string text, int x, int y ) {
		GUI.Label( new Rect( x, y, 100, 10), text, styleRegular );
	}

	public void DrawVLabel( string text, int x, int y ) {
		StringBuilder sb = new StringBuilder();

		foreach (char ch in text) {
			sb.Append(ch).Append("\n");
		}

		GUI.Label( new Rect( x, y, 100, 10), sb.ToString(), styleRegular );
	}
	#endregion


    void LoadFontStyles() {
		styleRegular = new GUIStyle();
		styleRegular.font = RulerResources.Instance.regularFont;
		styleRegular.fontSize = 9 * Rulers.resScaleFactor;
		styleRegular.normal.textColor = new Color(0.205f, 0.205f, 0.205f); 
		styleRegular.stretchWidth = false;
		styleRegular.stretchHeight = false;
		styleRegular.fontStyle = FontStyle.Normal;

		styleCoord = new GUIStyle();
		styleCoord.font = RulerResources.Instance.regularFont;
		styleCoord.fontSize = 9;
		styleCoord.normal.textColor = new Color(0.205f, 0.205f, 0.205f);
		styleCoord.stretchWidth = false;
		styleCoord.stretchHeight = false;
		styleCoord.fontStyle = FontStyle.Normal;
		styleCoord.border = new RectOffset( 5, 5, 5, 5 );
		styleCoord.padding = new RectOffset( 3, 3, 3, 2 );
	}
	

	GUIStyle styleRegular;
	GUIStyle styleCoord;
}
