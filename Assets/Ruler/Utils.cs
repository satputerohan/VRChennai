﻿
	using UnityEngine;
	
	public class Utils {
		
		static Utils instance;
		
		public static Utils Instance {
			get {
				if( instance == null ) {
					instance = new Utils(); 
				}
				return instance;
			}
		}

		ContextInfo contextInfo;

		private Utils() {
			contextInfo = ContextInfo.Instance;
		}

		public Vector2 GetWorldToScreenPixelCoord( Vector3 worldCoord ) {
			return contextInfo.cam.WorldToScreenPoint( worldCoord );
		}

		public Vector2 GetWorldCoord( Vector2 pixelCoord ) {
			return contextInfo.cam.ScreenToWorldPoint( pixelCoord );
		}

		public float GetWorldToContextX( float x ) {
			x -= contextInfo.origin.x;
			x /= contextInfo.scale.x;

			return x;
		}
		
		public float GetWorldToContextY( float y ) {
			y -= contextInfo.origin.y;
			y /= contextInfo.scale.y;
			
			return y;
		}

	}
