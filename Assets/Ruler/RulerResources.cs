﻿
using UnityEngine;

public class RulerResources {
	static RulerResources instance;
		
	public static RulerResources Instance {
		get {
			if( instance == null ) {
				instance = new RulerResources();
			}
			return instance;
		}
	}

	public Texture2D rulerHBg;
	public Texture2D rulerVBg;
	public Texture2D rulerLinePixel;
	public Texture2D corner;
	public Texture2D pixel;
	public Texture logo;
	public Font regularFont;


	private RulerResources() {

		rulerHBg 		= LoadTexture2D( pathRulerHBg );
		rulerVBg 		= LoadTexture2D( pathRulerVBg );
		rulerLinePixel 	= LoadTexture2D( pathRulerLinePixel );
		corner			= LoadTexture2D( pathImgCorner );
		pixel 			= LoadTexture2D( pathImgPixel );

		regularFont = LoadFont( pathFntRegular );
	}

	Texture2D LoadTexture2D( string textureName ) {
        return Resources.Load(textureName) as Texture2D;
    }

	Texture LoadTexture( string textureName ) {
		return Resources.Load<Texture>( textureName);
	}

	Font LoadFont( string fontName ) {
		return Resources.Load<Font>( fontName);
	}

	Sprite LoadSprite( string spriteName ) {
		return Resources.Load(spriteName, typeof(Sprite) ) as Sprite;
	}
		
	string pathRulerHBg			= "R2DImgRulerHBg";
	string pathRulerVBg			= "R2DImgRulerVBg";
	string pathRulerLinePixel 	= "R2DImgRulerLinePixel";
	string pathImgCorner		= "R2DImgCorner";
	string pathImgPixel			= "R2DImgPixel";
	string pathFntRegular		= "R2DFntMain";
}
